#!/bin/bash
#  nhs-login.sh
#  Copyright held by HCRS, 2019 <https://gitlab.com/hcrs>
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <https://www.gnu.org/licenses/>.

# == USER TUNABLES START ==
# Network name, as seen in NetworkManager
NETWORK_NAME="wifi";

# Username
UNAME="user";

# Password
PSWRD="password";

# Form URL
FORM="https://hostname.tld/auth/action/ads"

# Logfile
LOG="/var/log/nhs-login.log";
# == USER TUNABLES END ==

# Get uuid from network name
_uuid=$(grep uuid "/etc/NetworkManager/system-connections/$NETWORK_NAME" | sed "{s/uuid=//}"); 
# check if current conn is netic network
if [[ $CONNECTION_UUID == $_uuid ]] ; then
	# Only run if interface is being brought up
	if [[ $2 == "up" ]] ; then
		# Grab date string
		_date=$(date --iso-8601=seconds);
		printf "$_date\nConnected to $NETWORK_NAME.\nAttempting to login to NHS...\n" >> $LOG
		# Check if we're held captive
		_ua="User-Agent: Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:60.0) Gecko/20100101 Firefox/60.0";
		_portal=$(curl -v -H $_ua http://detectportal.firefox.com/success.txt 2>&1 | grep -i netic | wc -l);
		# If we are, submit.
		if [[ $_portal -ne 0 ]] ; then
			echo "Submitting login form."  >> $LOG
			# curl submit with Firefox headers
			curl \
			-X POST \
			-H  $_ua \
			-H "Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8" \
			-H "Accept-Language: en-US,en;q=0.5" \
			-H "Accept-Encoding: gzip, deflate, br" \
			-H "Content-Type: application/x-www-form-urlencoded" \
			-H "DNT: 1" \
			-H "Connection: keep-alive" \
			-H "Upgrade-Insecure-Requests: 1" \
			-H "Pragma: no-cache" \
			-H "Cache-Control: no-cache" \
			-d "username=$UNAME&password=$PSWRD" \
			$FORM
		else
			echo "Already logged in."  >> $LOG
		fi
		printf "\n" >> $LOG
	fi
fi
