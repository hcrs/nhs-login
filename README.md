NHS-Login
=========
Netic Hotspot Solution is a pain in the ass. There, I said it.
This login script for Netic Hotspot Solution aims to reduce
the frustration of connecting to networks with NHS captive portal enabled,
by automating the login procedure. To the server, there is no difference between
Mozilla Firefox submitting the form, and this script, since we use the same
request headers.

This solution is made by me, for me. It might work for you, but you will likely
need to tweak it. The requirements, as I see them, are a Debian 9 system with:
- NetworkManager
- bash
- awk
- date
- curl

To install, do the following:

0.  [**Download**](https://gitlab.com/hcrs/nhs-login/raw/master/nhs-login.sh?inline=false) the script.
1.  Read the entire script, understand it.
2.  Enter your login details in the appropriate variables.
3.  Enter the network name as seen in NetworkManager "Network Connections" menu.
4.  Enter correct form url
5.  Place `nhs-login.sh` in `/etc/NetworkManager/dispatcher.d/`
6.  `chown root:root /etc/NetworkManager/dispatcher.d/nhs-login.sh`
7.  `chmod +x /etc/NetworkManager/dispatcher.d/nhs-login.sh`
8.  Enjoy

Example for the **UCN Student** network:
```bash
# Network name, as seen in NetworkManager
NETWORK_NAME="UCN Student";

# Username
UNAME="1001234";

# Password
PSWRD="010119901001";

# Form URL
FORM="https://hotspot.ucn.dk/auth/action/ads"
```
**This software is GPL, see LICENSE.md for more info**
